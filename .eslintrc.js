module.exports = {
  root: true,
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint', 'jsx-a11y', 'react'
  ],
  parserOptions: {
    project: './tsconfig.json'
  },
  extends: [
    'eslint:recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'airbnb'
  ],
  env: {
    browser: true,
    jest: true
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['@api', './src/api/api.ts']
        ],
        extensions: ['.js', '.jsx', '.ts', '.tsx']
      },
      node: {
        extensions: ['.js', '.jsx', '.ts', '.tsx']
      }
    }
  },
  globals: {
    $: true,
    document: true,
    fetch: true,
    localStorage: true,
    mount: true,
    shallow: true,
    window: true
  },
  rules: {
    'linebreak-style': 'off',
    'comma-dangle': ['error', 'never'],
    'arrow-parens': ['error', 'always'],
    'camelcase': 'warn', // TODO: Set to error
    'function-paren-newline': ['error', 'multiline-arguments'], // if one attribute is one a new line, the first one should be to
    // Mute eslint to let @typescript-eslint trigger the errors
    'no-unused-vars': 'warn', // TODO: Set to warn
    'prefer-destructuring': 'error',
    'semi': 'off', // disallow use trailing semicolons
    'no-tabs': 'warn',
    'eol-last': ['error', 'never'],
    'indent': [
      'error',
      2,
      {
        VariableDeclarator: 2,
        SwitchCase: 1,
        MemberExpression: 1,
        FunctionDeclaration: { body: 1, parameters: 'first' },
        CallExpression: { arguments: 'first' },
        ArrayExpression: 'first',
        ObjectExpression: 'first',
        ImportDeclaration: 'first'
      }
    ],
    'no-multi-assign': 'warn', // TODO: Set to true
    'key-spacing': ['warn', { align: 'value' }],
    'max-len': [
      'error',
      {
        code: 160,
        ignoreStrings: true,
        ignoreTemplateLiterals: true,
        ignoreComments: true,
        ignoreUrls: true,
        ignoreRegExpLiterals: true
      }
    ],
    'newline-per-chained-call': 'off',
    'no-fallthrough': ['warn', { commentPattern: 'break[\\s\\w]*omitted' }], // warn if switch case without break, except there is comment with 'break omitted' in the case
    'no-multi-spaces': [
      'error',
      { exceptions: { ImportDeclaration: true, VariableDeclarator: true } }
    ], // cause we dont align by value, there should be no multispaces
    'no-unused-expressions': ['error', { allowTernary: true }],
    'object-curly-newline': ['error', { consistent: true }],
    'jsx-quotes': ['warn', 'prefer-single'],
    'class-methods-use-this': 'warn', // TODO: Set to error
    'padded-blocks': 'off',
    'arrow-body-style': ["error", "as-needed"],

    //// import rules -> https://github.com/benmosher/eslint-plugin-import
    'import/prefer-default-export': 0, // 3rd party librarys sometimes dont have a default export
    'import/extensions': [
      'error',
      {
        'ts': 'never',
        'tsx': 'never',
        'js': 'never',
        'jsx': 'never'
      }
    ],


    //// typescript rules -> https://github.com/typescript-eslint/typescript-eslint
    '@typescript-eslint/member-delimiter-style': ['error', {
      'multiline': {
        'delimiter': 'none',
        'requireLast': false
      },
    }],
    '@typescript-eslint/explicit-function-return-type': 'warn', // TODO: Set to error
    '@typescript-eslint/interface-name-prefix': 'warn',  // TODO: Set to error
    '@typescript-eslint/ban-ts-ignore': 'warn',
    '@typescript-eslint/consistent-type-definitions': 'warn',
    '@typescript-eslint/no-empty-interface': 'warn', // TODO: Set to error
    '@typescript-eslint/no-empty-function': 'warn', // TODO: Set to error
    '@typescript-eslint/no-unused-vars': 'warn', // TODO: Set to error


    //// jsx rules -> https://github.com/evcohen/eslint-plugin-jsx-a11y
    'jsx-a11y/anchor-has-content': ['warn', { aspects: ['invalidHref'] }],
    'jsx-a11y/anchor-is-valid': 'error',
    'jsx-a11y/click-events-have-key-events': 'off',
    // 'jsx-a11y/href-no-hash': 'off',  // removed from jsx-a11y
    'jsx-a11y/no-noninteractive-element-interactions': 'warn', // to increase future coding, raise a warning when onclick on somethin like a div tag
    'jsx-a11y/no-static-element-interactions': 'warn',
    // 'jsx-a11y/label-has-for': [2, { 'required': { 'every': ['id'] } }], // depracted
    'jsx-a11y/label-has-associated-control': [
      'error',
      {
        // replaces label-has-for
        labelAttributes: ['label'], // only check for lables
        assert: 'htmlFor', // check if htmlfor is present
        depth: 3
      }
    ],


    //// react rules -> https://github.com/yannickcr/eslint-plugin-react
    'react/jsx-filename-extension': ['warn', { extensions: ['.ts', '.tsx'] }],
    'react/prop-types': 'off', // would be nice but to many props are missing currently
    'react/require-default-props': 'warn', // TODO: Set to error
    'react/destructuring-assignment': 'warn',
    'react/jsx-props-no-spreading': 'warn', // TODO: Set to error
    'react/no-unused-state': 'warn' // TODO: Set to error
  }
}
