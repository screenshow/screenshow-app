import * as Superagent from 'superagent'

// IMPORT APP SETTINGS ZONE
import AppSettings from '../appSettings'

// TYPES
export type HttpProtocol = 'GET' | 'POST'

export default class Api {
  private PATH_TO_API: string;

  constructor() {
    this.PATH_TO_API = AppSettings.settings.api[AppSettings.settings.api_to_use]
    this.setRestFacades()
  }

  private setRestFacades(): void {
    const options = {
      headers: {
        Authorization: 'Bearer token'
      },
      errorFormatter: {
        name: 'error.title',
        message: 'error.text'
      }
    }
  }

  public request(protocol: HttpProtocol, requestPath: string): Superagent.Request {
    switch (protocol) {
      case 'GET':
        return Superagent.get(this.PATH_TO_API + requestPath)

      case 'POST':
        return Superagent.post(this.PATH_TO_API + requestPath)

      default:
        return Superagent.get(this.PATH_TO_API + requestPath)
    }
  }
}