import * as React from 'react'
import * as ReactDOM from 'react-dom'

import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'

import { Application } from 'react-rainbow-components';

// IMPORT LIBRARIES ZONE
import './lib/jquery'
import './lib/fontawesome'
import 'bootstrap/dist/css/bootstrap.min.css'
// END IMPORT LIBRARIES ZONE

// IMPORT THEME
import theme from './theme'

// IMPORT STYLES ZONE
import './style.scss'
// END IMPORT STYLES ZONE


// IMPORT COMPONENTS ZONE

// IMPORT PAGES
import Home from './page/home/home'
import Show from './page/show/show'
import NoMatch from './page/noMatch/noMatch'

// IMPORT HELPER
import Helper from './helper'

// INTERFACES
interface IProps { }
interface IState {
  isSidebarOpen: boolean
}

// TODO : Change setState calling
// use -> this.setState((prevState: IState) => ({ test: "test" }))
// instead of -> this.setState({ test: "test" })

class App extends React.Component<IProps, IState> {

  constructor(props: IProps) {
    super(props)

    this.state = {
      isSidebarOpen: false
    }
  }

  componentDidMount(): void {
    this.init()
  }

  protected initLanguage = (): void => { }

  protected initCordovaPlugins = (): void => { }

  protected initUI(): void { }

  protected init() {
    this.initUI()
    this.initLanguage()
    this.initCordovaPlugins()
  }


  render(): JSX.Element {
    return (
      <Application theme={theme} >
        <Router>

          <main className='content-wrapper'>
            <Switch>
              <Route exact path='/' component={Home} />
              <Route path='/home' component={Home} />
              <Route path='/show' component={Show} />
              <Route path='*' component={NoMatch} />
            </Switch>
          </main>

        </Router>
      </Application >
    )
  }
}

ReactDOM.render(
  <App />,
  document.getElementById('app') as HTMLElement
)