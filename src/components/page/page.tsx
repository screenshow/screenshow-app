import * as React from 'react'

// IMPORT STYLES ZONE
import './page.scss'
import { Container } from 'react-bootstrap'
// END IMPORT STYLES ZONE

// IMPORT STORAGE HANDLER ZONE
// END IMPORT STORAGE HANDLER ZONE

// IMPORT COMPONENTS ZONE
// END IMPORT COMPONENTS ZONE

// IMPORT PAGES ZONE
// END IMPORT PAGES ZONE

// IMPORT IMAGES ZONE
// @ts-ignore
// END IMPORT IMAGES ZONE

// IMPORT HELPER ZONE
// END IMPORT HELPER ZONE

// IMPORT INTERFACE ZONE
// END IMPORT INTERFACE ZONE


interface IProps { }

interface IState { }

// TODO : Change setState calling
// use -> this.setState((prevState: IState) => ({ test: "test" }))
// instead of -> this.setState({ test: "test" })

export default class Page extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {}
  }

  componentDidMount(): void {
    this.init()
  }

  protected init() {
    this.initUI()
  }

  protected initUI(): void { }


  render(): React.ReactNode {
    return (
      <Container className='page p-3'>
        <div className='page-content'>
          {this.props.children}
        </div>
      </Container>
    )
  }
}