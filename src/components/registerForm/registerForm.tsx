import * as React from 'react'
import { Card, Input, Button } from 'react-rainbow-components'
import { Container, Row, Col } from 'react-bootstrap'
import { Link, withRouter, Redirect } from 'react-router-dom'
import Api from '@api'
import Icon from '../Icon/Icon'

import styled from 'styled-components';


import './registerForm.scss'

interface Props { }
interface State {
  email: string
  password: string
  confirmationPassword: string
  isFormValid: boolean
  isSignUpButtonLoading: boolean
}

// STYLES
const StylesRegisterForm = styled(Container)`
  color: red !important;

  &:hover {
   background-color: yellow;
 }
`;

export default class RegisterForm extends React.Component<Props, State> {
  private api: Api

  constructor(props: Props) {
    super(props)

    this.state = {
      email: '',
      password: '',
      confirmationPassword: '',
      isFormValid: false,
      isSignUpButtonLoading: false
    }

    this.api = new Api()


    /*
    fetch('http://127.0.0.1:8000/auth/register')
      .then((response) => {
        return response.json();
      })

      this.api.request('POST', '/auth/register').send({
        name: this.state.confirmationPassword,
        email: this.state.email,
        password: this.state.password
      }).type('application/json').end((err: any, res: any) => {
        console.log(res)
        console.log(err)
      })
     */
  }

  private handleSubmit = (event: React.MouseEvent) => {
    this.setState((prevState: State) => ({
      isSignUpButtonLoading: true
    }))

    this.api.request('POST', '/auth/register').send({
      name: this.state.confirmationPassword,
      email: this.state.email,
      password: this.state.password
    }).end((err: any, res: any) => {
      console.log(res)
      console.log(err)
    })
  }

  private setEmailInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;

    this.setState(() => ({
      email: value
    }))
  }

  private setPasswordInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;

    this.setState(() => ({
      password: value
    }))
  }

  private setConfirmationPasswordInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;

    this.setState(() => ({
      confirmationPassword: value
    }))
  }


  render() {
    const { isSignUpButtonLoading, isFormValid } = this.state

    return (
      <StylesRegisterForm id='signInForm'>
        {isFormValid ? <Redirect to='/' /> : ''}

        <Row>
          <Col className='m4'>
            <Card className='d-flex flex-column align-items-center'>
              <div className='react-rainbow-admin-forms_header'>
                <img src='/assets/images/rainbow-logo.svg' alt='rainbow logo' className='react-rainbow-admin-forms_logo' />
                <h1>Sign up</h1>
              </div>
              <div className='d-flex flex-column'>
                <Input
                  icon={<Icon icon='envelope' />}
                  name='email'
                  label='Email'
                  required
                  placeholder='Enter your email'
                  type='email'
                  value={this.state.email}
                  onChange={this.setEmailInput}
                />
                <Input
                  icon={<Icon icon='lock' />}
                  name='password'
                  label='Password'
                  required
                  placeholder='Enter your password'
                  type='password'
                  value={this.state.password}
                  onChange={this.setPasswordInput}
                />
                <Input
                  icon={<Icon icon='lock' />}
                  name='password'
                  label='Password'
                  required
                  placeholder='Enter your password'
                  type='password'
                  value={this.state.confirmationPassword}
                  onChange={this.setConfirmationPasswordInput}
                />
                <Button
                  isLoading={isSignUpButtonLoading}
                  onClick={this.handleSubmit}
                  className='rainbow-m-top_medium'
                  type='submit'
                  variant='brand'
                >
                  <span>Create account</span>
                </Button>
                <div>
                  <p>Already have an account?</p>
                  <Link to='/login'>Log in</Link>
                </div>
              </div>
            </Card>
          </Col>
        </Row>
      </StylesRegisterForm>
    )
  }
}