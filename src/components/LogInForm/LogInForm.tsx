import * as React from 'react'
import { Card, Input, Button } from 'react-rainbow-components'
import { Container, Row, Col } from 'react-bootstrap'
import { Link } from 'react-router-dom'
import Icon from '../Icon/Icon'

import Api from '../../api/api'

import './LogInForm.scss'

interface Props { }
interface State {
  isSignInButtonLoading: boolean
}

export default class LogInForm extends React.Component<Props, State> {
  private api: Api

  constructor(props: Props) {
    super(props)

    this.state = {
      isSignInButtonLoading: false
    }

    this.api = new Api()
  }

  private handleSubmit = (event: React.MouseEvent) => {
    this.setState((prevState: State) => ({
      isSignInButtonLoading: true
    }))

    this.api.request('POST', '/auth/login').then((console.log))
  }

  render() {
    const { isSignInButtonLoading } = this.state

    return (
      <Container id='signInForm'>
        <Row>
          <Col className='m4'>
            <Card className='d-flex flex-column align-items-center'>
              <div className='react-rainbow-admin-forms_header'>
                <img src='/assets/images/rainbow-logo.svg' alt='rainbow logo' className='react-rainbow-admin-forms_logo' />
                <h1>Log in</h1>
              </div>
              <div className='d-flex flex-column'>
                <Input
                  icon={<Icon icon='envelope' />}
                  name='email'
                  label='Email'
                  required
                  placeholder='Enter your email'
                  type='email'
                />
                <Input
                  icon={<Icon icon='lock' />}
                  name='password'
                  label='Password'
                  required
                  placeholder='Enter your password'
                  type='password'
                />
                <Button
                  isLoading={isSignInButtonLoading}
                  onClick={this.handleSubmit}
                  className='rainbow-m-top_medium'
                  type='submit'
                  variant='brand'
                >
                  <span>Login</span>
                </Button>
                <div>
                  <p>Don’t have an account?</p>
                  <Link to='/register'>Create Account</Link>
                  <p>Forgot your password?</p>
                </div>
              </div>
            </Card>
          </Col>
        </Row>
      </Container>
    )
  }
}