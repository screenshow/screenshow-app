import * as React from 'react'

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from 'react-router-dom'


// IMPORT STORAGE HANDLER ZONE
// END IMPORT STORAGE HANDLER ZONE

// IMPORT CORDOVA PLUGINS
// END IMPORT CORDOVA PLUGINS

// IMPORT COMPONENTS ZONE
// END IMPORT COMPONENTS ZONE

// IMPORT PAGES ZONE
// END IMPORT PAGES ZONE

// IMPORT IMAGES ZONE
// @ts-ignore
// END IMPORT IMAGES ZONE

// IMPORT HELPER ZONE
// END IMPORT HELPER ZONE

// IMPORT INTERFACE ZONE
// END IMPORT INTERFACE ZONE


interface IProps { }

interface IState { }

export default class Show extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props)
    this.state = {}
  }

  componentDidMount(): void {
    this.init()
  }

  protected init() {
    this.initUI()
  }

  protected initUI(): void { }


  render(): React.ReactNode {
    return (
      <div id='showPage'>
        <div id='visual' />
      </div>
    )
  }
}