import * as React from 'react'


// IMPORT CORDOVA PLUGINS
// END IMPORT CORDOVA PLUGINS

// IMPORT COMPONENTS ZONE
import { Tabset, Tab, ButtonGroup, ButtonIcon } from 'react-rainbow-components'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Page from '../../components/page/page'
import SignIn from '../../components/LogInForm/LogInForm'
// END IMPORT COMPONENTS ZONE

// IMPORT PAGES ZONE
// END IMPORT PAGES ZONE

// IMPORT IMAGES ZONE
// @ts-ignore
// END IMPORT IMAGES ZONE

// IMPORT HELPER ZONE
// END IMPORT HELPER ZONE

// IMPORT INTERFACE ZONE
// END IMPORT INTERFACE ZONE


interface IProps { }

interface IState {
  selected: string
}

// TODO : Change setState calling
// use -> this.setState((prevState: IState) => ({ test: "test" }))
// instead of -> this.setState({ test: "test" })

export default class Home extends React.Component<IProps, IState> {

  constructor(props: IProps) {
    super(props)

    this.state = {
      selected: 'recents'
    }
  }

  render(): JSX.Element {
    return (
      <Page>
        <SignIn />
      </Page>
    )
  }
}