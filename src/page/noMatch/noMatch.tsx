import * as React from 'react'


// IMPORT STYLES ZONE
import './noMatch.scss'
// END IMPORT STYLES ZONE

// IMPORT STORAGE HANDLER ZONE
// END IMPORT STORAGE HANDLER ZONE

// IMPORT CORDOVA PLUGINS
// END IMPORT CORDOVA PLUGINS

// IMPORT COMPONENTS ZONE
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import Page from '../../components/page/page'
// END IMPORT COMPONENTS ZONE

// IMPORT PAGES ZONE
// END IMPORT PAGES ZONE

// IMPORT IMAGES ZONE
// @ts-ignore
// END IMPORT IMAGES ZONE

// IMPORT HELPER ZONE
// END IMPORT HELPER ZONE

// IMPORT INTERFACE ZONE
// END IMPORT INTERFACE ZONE


interface IProps { }

interface IState { }

// TODO : Change setState calling
// use -> this.setState((prevState: IState) => ({ test: "test" }))
// instead of -> this.setState({ test: "test" })

export default class Home extends React.Component<IProps, IState> {

  constructor(props: IProps) {
    super(props)
    this.state = {}
  }


  render(): JSX.Element {
    return (
      <Page>
        <h1>404 not found</h1>
      </Page>
    )
  }
}