// IMPORT APP SETTINGS ZONE
import SETTINGS from '../app-settings.json'
// END IMPORT APP SETTINGS ZONE

// INTERFACE ZONE
export type TApiType = 'local' | 'server'

export interface IAppSettings {
    name: string
    version: string
    use_cordova: boolean
    api_to_use: TApiType
    api: {
        local: string
        server: string
    }
}
// END INTERFACE ZONE

export default class AppSettings {
    public static settings: IAppSettings = SETTINGS as IAppSettings;
}